%% @author Prakash Parmar
%% @doc @todo Add description to distributed_counter.


-module(distributed_counter).

-record(state, {operation}).

%% ====================================================================
%% API functions
%% ====================================================================
-export([
         init/2,
         allowed_methods/2,
         content_types_provided/2,
         content_types_accepted/2,
         counter/2
        ]).

-define(COUNTER_OBJ, {dist_counter, antidote_crdt_counter_pn, my_bucket}).

%% ====================================================================
%% Internal functions
%% ====================================================================

init(Req, Opts) ->
    [Op | _] = Opts,
    State = #state{operation=Op},
    {cowboy_rest, Req, State}.

%%--------------------------------------------------------------------

allowed_methods(Req, State) ->
    {[<<"GET">>, <<"POST">>], Req, State}.

%%--------------------------------------------------------------------

content_types_provided(Req, State) ->
    {[
      {<<"application/text">>, counter}
     ], Req, State}.

%%--------------------------------------------------------------------

content_types_accepted(Req, State) ->
    {[
      {<<"application/text">>, counter}
     ], Req, State}.

%%--------------------------------------------------------------------

counter(Req, #state{operation='get'} = State) ->
    
    io:format("~n[~w:~p] GET request.~n",[?MODULE, ?LINE]),
    
    {_, Resp} = read_counter(ignore), 
    
    io:format("~n[~w:~p] GET response : ~p~n",[?MODULE, ?LINE, Resp]),
    
    {Resp, Req, State};
  
counter(Req, #state{operation='increment'} = State) ->
    
%%     Value = cowboy_req:binding(val, Req),
%%     io:format("~n[~w:~p] Increment. Val : ~p~n",[?MODULE, ?LINE, Value]),
    
    io:format("~n[~w:~p] POST request - increment.~n",[?MODULE, ?LINE]),
    Resp =  case update_counter(increment, 1) of
                {success, Vector_clk} ->
                    {_, Result} = read_counter(Vector_clk),
                    Result;
                {error, Reason} -> Reason
            end,
    io:format("~n[~w:~p] POST response : ~p~n",[?MODULE, ?LINE, Resp]),
    
    Req1 = cowboy_req:set_resp_body(Resp, Req),
    
    {true, Req1, State};

counter(Req, #state{operation='decrement'} = State) ->
    
%%     Value = cowboy_req:binding(val, Req),
%%     io:format("~n[~w:~p] Decrement. Val : ~p~n",[?MODULE, ?LINE, Value]),
    
    io:format("~n[~w:~p] POST request - decrement. ~n",[?MODULE, ?LINE]),
    Resp =  case update_counter(decrement, 1) of
                {success, Vector_clk} ->
                    {_, Result} = read_counter(Vector_clk),
                    Result;
                {error, Reason} -> Reason
            end,
    io:format("~n[~w:~p] POST response : ~p~n",[?MODULE, ?LINE, Resp]),
    
    Req1 = cowboy_req:set_resp_body(Resp, Req),
    
    {true, Req1, State}.

%%--------------------------------------------------------------------

update_counter(Operation, Value) ->
    
    case node_manager:which_node() of
        'not_available' -> 
            {error, erlang:list_to_binary("Error : Either Connection to DB node or DB node is Down.\n")};
        
        Node when is_atom(Node) ->
            io:format("~n[~w:~p] Querying to node : ~p~n",[?MODULE, ?LINE, Node]),
            Response = rpc:call(Node, 
                                antidote, 
                                update_objects, 
                                [ignore, [], [{?COUNTER_OBJ, Operation, Value}]]),
            case Response of
                {ok, Vector_clk} ->
                    {success, Vector_clk};
                
                {error, Reason} ->
                    io:format("~n[~w:~p] Update Failed. Reason : ~p~n",[?MODULE, ?LINE, Reason]),
                    {error, erlang:list_to_binary("Error : Update Failed.\n")};
                
                {badrpc,nodedown} ->
                    io:format("~n[~w:~p] Update Failed. Database node is Down.~n",[?MODULE, ?LINE]),
                    {error, erlang:list_to_binary("Error : Node Down.\n")}
            end;
        Error ->
            io:format("~n[~w:~p] Got un-expected error. Error : ~p~n",[?MODULE, ?LINE, Error]),
            {error, erlang:list_to_binary("Error : un-expected error.\n")}
    end.

%%--------------------------------------------------------------------

read_counter(Vector_clk) ->
    
    case node_manager:which_node() of
        'not_available' -> 
            {error, erlang:list_to_binary("Error : Either Connection to DB node or DB node is Down.\n")};
        
        Node when is_atom(Node) ->
            io:format("~n[~w:~p] Querying to node : ~p~n",[?MODULE, ?LINE, Node]),
            Response = rpc:call(Node, 
                                antidote, 
                                read_objects, [Vector_clk, [], [?COUNTER_OBJ]]),
            
            case Response of
                {ok, [Val|_], _Vector_clk} ->
                    {success, erlang:integer_to_binary(Val) };
                
                {error, Reason} ->
                    io:format("~n[~w:~p] Read Failed. Reason : ~p~n",[?MODULE, ?LINE, Reason]),
                    {error, erlang:list_to_binary("Error : Read Failed.\n")};
                
                {badrpc,nodedown} ->
                    io:format("~n[~w:~p] Read Failed. Database node is Down.~n",[?MODULE, ?LINE]),
                    {error, erlang:list_to_binary("Error : Node Down.\n")}
                    
            end;
        Error ->
            io:format("~n[~w:~p] Got un-expected error. Error : ~p~n",[?MODULE, ?LINE, Error]),
            {error, erlang:list_to_binary("Error : un-expected error.\n")}
    end.
