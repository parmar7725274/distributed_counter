%%%-------------------------------------------------------------------
%% @doc distributed_counter top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(distributed_counter_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: #{id => Id, start => {M, F, A}}
%% Optional keys are restart, shutdown, type, modules.
%% Before OTP 18 tuples must be used to specify a child. e.g.
%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
init([]) ->
    Node_manager = #{id         => node_manager,     
                     start      => {node_manager, start_link, []},    
                     restart    => permanent,  
                     shutdown   => 5000, 
                     type       => worker,       
                     modules    => [node_manager] },  
    
    {ok, {{one_for_all, 0, 1}, [ Node_manager ]}}.

%%====================================================================
%% Internal functions
%%====================================================================
